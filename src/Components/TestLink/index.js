import React from 'react';

import IconNew from 'static/icons/new.png';

import './styles.scss';

const TestLink = ({
  title,
  links,
}) => (
  <div className="test-link mb-4">
    <h3 className="text-center">
      <b>
        {title}
      </b>
    </h3>
      {
        links.map(
          (test, index) => 
          <div>
            <a href={test.link}>
              <span>👉</span> <i>{test.link}</i> { index === 0 && <img src={IconNew} alt="new"/>}
            </a>
          </div>
        )
      }
  </div>
)

export default TestLink;
