import React from 'react';

import './styles.scss';

const DateItem = ({
    date,
    info,
}) => (
  <div className="importantDate">
    <h3>
      {date}
    </h3>
    <h4>
      {info}
    </h4>
  </div>
);

const ImportantDates = ({
    title,
    importantDateData,
}) => (
  <div className="importantDateWrapper">
    <h3 className="title">{title}</h3>
    { importantDateData.map(
      dateData => <DateItem key={dateData.date} date={dateData.date} info={dateData.info}/>
    )}
  </div>
)

export default ImportantDates;


