import React, { useMemo } from 'react';
import {
  Container,
} from 'react-bootstrap';

import './styles.scss';

import LeftArrow from 'static/icons/left.png';
import RightArrow from 'static/icons/right.png';

import { padZero } from 'utils.js';

const MarkDownText = ({
  date,
  month,
  year,
  onDateConfirm,
  onChangeDate
}) => {
  const DatePicker = useMemo(
    () => {
      if (date) {
        return <input
          type="date"
          value={`${year}-${padZero(month)}-${padZero(date)}`}
          onInput={(e) => onDateConfirm(e.target.value)}
        />
      }
        return <input
          type="month"
          id="month"
          value={`${year}-${padZero(month)}`}
          onInput={(e) => onDateConfirm(e.target.value)}
        />
    }, [date, month, onDateConfirm, year]
  )

  return (
    <Container className="CANavigation">
      {DatePicker}
      <div>
        <img
          onClick={() => onChangeDate(-1)} 
          src={LeftArrow}
          alt="Goto Previous"
        />
        <img 
          onClick={() => onChangeDate(1)} 
          src={RightArrow}
          alt="Goto Next"
        />
      </div>
    </Container>
  );
}

export default MarkDownText;
