

















  ## ✅ हाल ही में छत्तीसगढ़ राज्य में 3 दिवसीय राष्ट्रीय जनजातीय नृत्य महोत्सव का आयोजन किया गया है
![रायपुर में 28 से 30 अक्टूबर तक होगा राष्ट्रीय आदिवासी नृत्य महोत्सव का आयोजन](https://indiapublickhabar.in/wp-content/uploads/2021/10/tribal.png)

  ## ✅ हाल ही में ओडिशा राज्य के भुवनेश्वर शहर ने जूनियर पुरुष हॉकी विश्व कप की मेजबानी की है
![Odisha District Map, Odisha Political Map](https://www.burningcompass.com/countries/india/maps/odisha-district-map.jpg)

  ## ✅ हाल ही में भारतीय सेना ने चीन के विरुद्ध प्लान 190 योजना की शुरुआत की है
![Defence Updates #1454 - IAF Mirage-2000 Crash, Army Anti-Tank Drill, Indian  Army Plan 190, India UN - YouTube](https://i.ytimg.com/vi/-M16siJIh40/maxresdefault.jpg)

  ## ✅ हाल ही में अमेरिका देश में साल्मोनेला बैक्टीरिया के सैकड़ो मामले दर्ज किए गए है
![after covid Salmonella outbreak in america is new health concern know what  is Salmonella bacteria symptoms and causes and treatment - अगर आप भी खाते  हैं कच्चा प्याज तो बन सकते हैं](https://images1.livehindustan.com/uploadimage/library/2021/10/27/16_9/16_9_1/salmonella_bacteria_1635321892.jpg)

  ## ✅ हाल ही में जी किशन रेड्डी ने अमृत महोत्सव पॉडकास्ट लांच किया है
![Culture Minister G.K Reddy launches Amrit Mahotsav Podcast](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2021/10/27075202/Union-Minister-G-Kishan-Reddy-launches-the-Amrit-Mahotsav-Podcast.jpg)

  ## ✅ हाल ही में ब्रिटेन के ग्लास्गो शहर ने COP-26 शिखर सम्मलेन की मेजबानी की है
![UN Climate Change Conference (UNFCCC COP 26) | La Communauté du Pacifique](https://www.spc.int/sites/default/files/styles/featured_image/public/featuredimages/events/2020-12/COP26-Glasgow-2021-1.jpg?itok=rafbjFvv)

  ## ✅ हाल ही में केंद्र सरकार ने भारत में कुल 7 PM MITRA पार्क बनाने की घोषणा की है
![Archives for pm-mitra | www.narendramodi.in |](https://cdn.narendramodi.in/cmsuploads/0.09903700_1633521535_government-approves-setting-up-of-7-pm-mitra-parks-with-a-total-outlay-of-rs-4-445-crore.jpeg)




  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/QRgPHQGH9dW4EBKS7
