





  ## ✅ हाल ही में प्रधानमंत्री नरेंद्र मोदी ने उत्तर प्रदेश राज्य में पूर्वांचल एक्सप्रेसवे का उदघाटन किया है
![पूर्वांचल एक्सप्रेस-वे की पूरी जानकारी || Purvanchal Expressway Route &amp;  Details - Quick News - YouTube](https://i.ytimg.com/vi/60REY3h3cgw/maxresdefault.jpg)

  ## ✅ हाल ही में भारत और बांग्लादेश देश की सेना ने संयुक्त साइकिल रैली का आयोजन किया है
![Epaar Bangla, opaar Bangla | Dhaka Tribune](https://media-eng.dhakatribune.com/uploads/2019/06/partha-bigstock-1560767749247.jpg)

  ## ✅ हाल ही में आयोजित COP-26 सम्मलेन में ग्लोबल वार्मिंग को 1.5 डिग्री तक सिमित रखने का लक्ष्य रखा है
![COP26 in Glasgow | Germanwatch e.V.](https://germanwatch.org/sites/default/files/styles/header_image/public/cop26-titelbild.jpg?itok=X7IVVfNl)

  ## ✅ हाल ही में आयोजित ICC T-20 विश्व कप 2021 में डेविड वार्नर को 'प्लेयर ऑफ़ द सीरीज' घोषित किया गया है
![David Warner (@davidwarner31) / Twitter](https://pbs.twimg.com/media/FEQUEHNVgAQFUMd.jpg)

  ## ✅ हाल ही में थंजावुर तमिलनाडु में भारत का पहला खाद्य संग्रहालय खोला गया है
![Current Affairs 17 November 2021](https://affairscloud.com/assets/uploads/2021/11/Indias-first-Food-Museum-opens-in-Thanjavur.jpg)

  ## ✅ हाल ही में केंद्र सरकार ने रक्षा सचिव,गृह सचिव,RAW के सचिव और IB के सचिव का कार्यकाल 2 वर्षो के लिए बढ़ा दिया है
![For central government employees, flexi attendance option extended](https://images.livemint.com/img/2021/06/02/1600x900/Jitendra_Singh_Twitter_1622606554827_1622606567630.jpg)

  ## ✅ हाल ही में सौरभ कृपाल दिल्ली उच्च न्यायालय में भारत के पहले समलैंगिक(Gay) जज बने है
![SC ruled homosexuality is not an offence. So why is it penalising Saurabh  Kirpal for being gay?](https://s01.sgp1.cdn.digitaloceanspaces.com/article/157125-snvzaqvudz-1617527079.jpg)







  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/1hevTKBybTP2HEJJ8
