




















  ## ✅ हाल ही में फ्रांस देश ने सैन्य संचार उपग्रह 'सिराक्यूज 4ए' लांच किया है
![France Map | Maps of French Republic](https://ontheworldmap.com/france/map-of-france.jpg)

  ## ✅ हाल ही में प्रधानमंत्री नरेंद्र मोदी ने इटली देश में हो रहे G20 शिखर सम्मलेन में भाग लिया है
![G20 Summit: PM Modi and other world leaders pose for &#39;family photo&#39; with  frontline workers in Rome | Visuals - India News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/202110/AP10_30_2021_000116B_0_1200x768.jpeg?Og9W20rZL.8zuXZH2X_7aNTO8TF.y5Kp&size=770:433)

  ## ✅ हाल ही में महाराष्ट्र राज्य अपनी स्वयं की वन्यजीव कार्य योजना को लागू करने वाला भारत का पहला राज्य बन गया है
![Districts Map Maharashtra | India world map, Map, Geography map](https://i.pinimg.com/originals/aa/e3/45/aae34508a0a42128515535c5413a6c4d.jpg)

  ## ✅ हाल ही में भारत देश ने एशियाई विकास बैंक और एशियन इंफ्रास्ट्रक्चर इन्वेस्टमेंट बैंक से COVID-19 वैक्सीन खरीदने के लिए ऋण लिया है
![ADB, AIIB processing USD 2 billion loan for India to buy COVID-19 vaccines-  The New Indian Express](https://images.newindianexpress.com/uploads/user/imagelibrary/2021/6/8/w1200X800/COVID_Vaccine_PTI1212.jpg)

  ## ✅ हाल ही में भारतीय तटरक्षक जहाज 'सार्थक' को पोरबंदर गुजरात में रखा गया है
![भारतीय तटरक्षक जहाज &#39;सार्थक&#39; का उद्घाटन किया गया- GK in Hindi - सामान्य  ज्ञान एवं करेंट अफेयर्स](https://i0.wp.com/hindi.gktoday.in/wp-content/uploads/2021/10/indian-coast-guard-ship-sarthak.png?fit=1200%2C675&ssl=1)

  ## ✅ हाल ही में विश्व बैंक संगठन के 'द चेंजिंग वेल्थ ऑफ़ नेशन 2021' रिपोर्ट जारी की है
![World Bank agrees to give $100mn to SL for Covid battle](https://gumlet.assettype.com/freepressjournal/2021-09/034dcec4-fbc7-4dad-b703-7f45423f62c2/WORLD_BANK.jpg?format=webp&w=400&dpr=2.6)

  ## ✅ हाल ही में उत्तर प्रदेश राज्य सरकार ने राम कथा पार्क का नाम बदलकर 'क्वीन हीयो हवांग ओके' मेमोरियल पार्क रख दिया है
![UP District Map, UP Political Map, Uttar Pradesh Political Map](https://www.burningcompass.com/countries/india/maps/uttar-pradesh-district-map.jpg)

  ## ✅ हाल ही में गुजरात राज्य सरकार ने 'जो ग्रीन' मिशन लांच किया है
![7th Go Green day celebration | Jain (Deemed-to-be University)](https://www.jainuniversity.ac.in/uploads/blog/89e8476914fc5f6bd0ec23d86118c741.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/1hevTKBybTP2HEJJ8
