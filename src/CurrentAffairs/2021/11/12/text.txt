


















  ## ✅ हाल ही में SBI बैंक ने पेंशनधारियो के लिए 'ऑनलाइन वीडियो जीवन प्रमाणपत्र' सेवा की शुरुआत की है
![SBI Video Life Certificate For Pensioners: How VLC process works - Step by  step guide | Zee Business](https://cdn.zeebiz.com/sites/default/files/styles/zeebiz_850x478/public/2021/11/02/164744-sbi1.jpg?itok=D7klFI3I)

  ## ✅ हाल ही में संकल्प गुप्ता भारत के सबसे तेज़/71वे ग्रैंडमास्टर बने है
![IM at the age of 15 - the story of Sankalp Gupta - YouTube](https://i.ytimg.com/vi/cJeGHKJ2gbA/maxresdefault.jpg)

  ## ✅ हाल ही में भारतीय वैज्ञानिक संस्थान ने कुल 17 भारतीय विज्ञानिको को स्वर्णजयंती फेलोशिप से सम्मानित किया है
![Swarnajayanti Fellowships are given to 17 Indian scientists for research  ideas and potential](https://pbs.twimg.com/media/FDvcFPdagAEHN2J?format=jpg&name=large)

  ## ✅ हाल ही में बंधन बैंक ने जुबीन गर्ग को असम राज्य में अपना ब्रांड एम्बेसडर बनाया है
![Current Affairs Today 2020 - AffairsCloud Today in English](https://affairscloud.com/assets/uploads/2021/11/Bandhan-Bank-ropes-in-Zubeen-Garg.jpg)

  ## ✅ हाल ही में अमेरिका देश ने समलैंगिक अधिकार कार्यकर्ता 'हार्वे मिल्क' के नाम पर एक जहाज़ लांच किया है
![Harvey Milk: US Navy launches ship named for gay rights leader - BBC News](https://ichef.bbci.co.uk/news/976/cpsprodpb/1786E/production/_121466369_gettyimages-1236396733.jpg)

  ## ✅ हाल ही में जारी महिला और बाल विकास मंत्रालय के आंकड़ों के अनुसार भारत में कुल 33 लाख कुपोषण बच्चो की संख्या निर्धारित की गयी है
![भारत में 33 लाख से अधिक बच्चे कुपोषण के शिकार, बिहार की स्थिति भी चिंताजनक](https://res.cloudinary.com/cloudinary014/image/upload/w_827,h_465,q_auto/v1636301709/news/cover/Nov2021/n4n688233e8-4b83-4fdb-ac50-463a809562cd.jpg)

  ## ✅ हाल ही में दिल्ली सरकार ने वायु प्रदुषण को कम करने के लिए पानी के छिड़काव के आपातकालीन प्रयोग को मंजूरी दी है
![Delhi: As pollution peaks, govt deploys 114 tankers to settle dust by  sprinkling water](https://images.livemint.com/img/2021/11/06/600x338/PTI11-06-2021-000041A-0_1636189427959_1636189438871.jpg)





  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/1hevTKBybTP2HEJJ8
