








  ## ✅ हाल ही में IIT दिल्ली ने पानी और बारिश की बूंदो से बिजली पैदा करने का उपकरण विकसित किया है 
![IIT-Delhi to look for faculty in Ivy League universities](https://cdn.dnaindia.com/sites/default/files/styles/full/public/2016/12/07/526581-iit-delhi.jpg)

  ## ✅ हाल ही में हरमिलन कौर ने राष्ट्रीय ओपन एथलेटिक्स चैंपियनशिप में 1500 मीटर में राष्ट्रीय रिकॉर्ड बनाया है 
![Harmilan Kaur Bains bolts out of parents&#39; shadow to break Sunita Rani&#39;s  1500m record | Sports News,The Indian Express](https://images.indianexpress.com/2021/09/Untitled-design-3-2.jpg)

  ## ✅ हाल ही में भारत सरकार ने पैन कार्ड को आधार से जोड़ने की अंतिम सिमा को 31 मार्च 2022 तक बढ़ा दिया है 
![PAN-Aadhaar linking deadline extended! Your PAN may become inoperative if  not linked by this date - The Financial Express](https://images.financialexpress.com/2021/06/PA1.jpg)

  ## ✅ हाल ही में जारी FIFA रैंकिंग में बेल्जियम देश ने टॉप किया है 
![FIFA.com on Twitter: &quot;NEW #FIFARANKING 🇧🇪Belgium take outright lead 🔝  🇫🇷France just one point back 😮 🇬🇮Gibraltar soar ✈️ Learn more 👉  https://t.co/Gc0w88dc9W… https://t.co/NWiCa7UopH&quot;](https://pbs.twimg.com/media/DqVqqYAXgAEnMvr.jpg)

  ## ✅ हाल ही में इंदु मल्होत्रा को दिल्ली का लोकपाल नियुक्त किया गया है 
![Who is Justice Indu Malhotra That could not complete her farewell speech](https://new-img.patrika.com/upload/2021/03/12/justice_indu_6742287_835x547-m.jpg)






  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/G4i7wQs59Daj4P1s6


