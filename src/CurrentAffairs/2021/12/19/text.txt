




  ## ✅ हाल ही में कोलकाता के दुर्गापूजा त्यौहार को 'यूनेस्को की अमूर्त सांस्कृतिक विरासत सूचि' में शामिल किया गया है
![Durga Puja Gets UNESCO Intangible Heritage Tag | Proud Moment for Every  Indian - YouTube](https://i.ytimg.com/vi/zmGMaLLBmMs/maxresdefault.jpg)

  ## ✅ हाल ही में बांग्लादेश देश ने 16 दिसंबर 2021 को अपने गठन के 50 वर्ष पूरा होने पर 'स्वर्णिम जयंती' मनाई है
![Nagad to celebrate 50 achievements of Bangladesh&#39;s 50 years](https://www.tbsnews.net/sites/default/files/styles/big_3/public/images/2021/03/22/nagad_50_years_celebration_image.jpg?itok=7up_YmJM)

  ## ✅ हाल ही में मनोज मुकुंद नरवणे को 'चीफ ऑफ़ स्टाफ समिति' का चेयरमैन नियुक्त किया गया है
![Manoj Mukund Naravane To Take Charge As Army Chief Today](https://c.ndtvimg.com/2019-12/qclvtpi8_manoj-mukund-naravane_650x400_16_December_19.jpg)

  ## ✅ हाल ही में केंद्रीय मंत्रिमंडल ने विवाह के लिए महिलाओं की न्यूनतम उम्र 18 वर्ष से बढाकर 21 वर्ष रखने के प्रस्ताव को मंजूरी दी है
![Marriage legal age for Girl: 18 से बढ़ाकर 21 साल होगी लड़कियों की शादी की  न्यूनतम उम्र, जानें क्या होगा इसका असर? - News Adda](https://newsaddaa.in/wp-content/uploads/2021/12/kushinagar-hindi-news-dainik-jagran-34-696x365.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/gLJKrwLG8a7g8wiN9
