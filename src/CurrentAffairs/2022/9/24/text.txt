








  ## ✅ हाल ही में विजय कुमार सिंह भूतपूर्व सैनिक कल्याण विभाग के नए सचिव बने है
![श्री विजय कुमार सिंह ने सचिव, भूतपूर्व सैनिक कल्याण विभाग का कार्यभार ग्रहण  किया - Saahas Samachar News Website](https://www.saahassamachar.in/wp-content/uploads/2022/09/image00143Y0.jpg)

  ## ✅ हाल ही में कज़ाख़स्तान देश की राजधानी 'नूरसुल्तान' का नाम बदलकर 'अस्ताना' रख दिया गया है
![File:Kazakhstan political map 2000.jpg - Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/8/87/Kazakhstan_political_map_2000.jpg)

  ## ✅ हाल ही में धर्मशाला(हिमाचल प्रदेश) शहर में राज्यों के पर्यटन मंत्रियो का 'राष्ट्रीय सम्मलेन 2022' आयोजित हुआ है
![Himachal Pradesh Map, Districts in Himachal Pradesh | India world map, Himachal  pradesh, Map](https://i.pinimg.com/736x/bb/da/ec/bbdaecb2d9801fa5b549b77aaae635ef.jpg)

  ## ✅ हाल ही में United Nation Environment Program संस्था ने समुंद्री पर्यटन को बढ़ावा देने के लिए 'ग्रीन फिन्स हब' नामक वैश्विक डिजिटल प्लेटफॉर्म को लांच किया है
![United Nations Environment Programme (UNEP) | GSI](https://www.iisd.org/gsi/sites/default/files/styles/wide_1300x650/public/2017-08/united-nations-environment-programme.jpg?h=34506367&itok=ukT7Nety)

  ## ✅ हाल ही में भारत देश चीन देश को पीछे छोड़कर 'श्रीलंका' देश के सबसे बड़े द्विपक्षीय ऋणदाता के रूप में उभरा है
![Sri Lanka and South India Administrative Map Stock Vector - Illustration of  indian, land: 82751181](https://thumbs.dreamstime.com/z/sri-lanka-south-india-administrative-map-political-82751181.jpg)




  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/hy8w5gyZUrK8LFi96
