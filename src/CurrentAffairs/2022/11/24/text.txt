






  ## ✅ हाल ही में अंतराष्ट्रीय परिवार नियोजन सम्मलेन में किस देश को परिवार नियोजन की राष्ट्र श्रेणी उत्कृष्टता पुरस्कार 2022 से नवाजा गया है- भारत
![Virt - International Conference on Family Planning | Your guide to the  world's most meaningful events](https://virtpublic.s3-us-east-2.amazonaws.com/2021/04/virt-photos-4-2.png)

  ## ✅ सेल स्वर्ण जयंती कहानी लेखन प्रतियोगिता-2022 में हिंदी वर्ग का पहला पुरस्कार किसे दिया गया है- डानी प्रसाद शर्मा
![Former Union Minister And Samajwadi Party Leader Beni Prasad Verma Passes  Away - पूर्व केंद्रीय मंत्री और समाजवादी पार्टी के नेता बेनी प्रसाद वर्मा  का निधन, सपा प्रमुख अखिलेश ...](https://c.ndtvimg.com/2020-03/g0bgmdvc_beni-prasad-verma-twitter_625x300_27_March_20.jpg?im=Resize=(1230,900))

  ## ✅ किसने बिल्ली जीन किंग कप 2022 का खिताब जीता है- स्विट्ज़रलैंड
![Switzerland Map and Satellite Image](https://geology.com/world/switzerland-map.gif)

  ## ✅ किस राज्य सरकार ने अपने राज्य में रामायण और महाभारत सर्किट विकसित करने की घोषणा की है- उत्तर प्रदेश
![Yogi cabinet approves 24 proposals Ramayana and Mahabharata circuit to be  built | योगी कैबिनेट में 24 प्रस्तावों को मिली मंजूरी, बनेंगे रामायण और महाभारत  सर्किट, पढ़िए खास ...](https://image.tricitytoday.com/thumb/550x368/yogi-39535.jpg)

  ## ✅ किसको मेटा के न्यू इंडिया हेड के रूप में नियुक्त किया गया है- संध्या देवनाथन
![संध्या देवनाथन। Who is Sandhya Devanathan। Meta India Head -कौन हैं Sandhya  Devanathan जिन्हें मेटा ने बनाया इंडिया हेड?](https://images.herzindagi.info/image/2022/Nov/meta-head-sandhya-devnathan.jpg)

  ## ✅ हाल ही में कैम्ब्रिज डिक्शनरी ने किस शब्द को 'वर्ड ऑफ़ द ईयर 2022' घोषित किया है- Homer
![9,481 Homer Images, Stock Photos & Vectors | Shutterstock](https://www.shutterstock.com/image-vector/homer-colorful-typography-text-banner-260nw-1858379317.jpg)

  ## ✅ कौनसा राज्य एलीफैंट डेथ ऑडिट फ्रेमवर्क लाने वाला देश का पहला राज्य बन गया है- तमिलनाडु
![Tamil Nadu District Map, Tamil Nadu Political Map](https://www.burningcompass.com/countries/india/maps/tamil-nadu-district-map.jpg)

  ## ✅ किसने भारत का पहला निजी रॉकेट डिज़ाइन किया है- Skyroot Aerospace
![Skyroot Aerospace to launch India's first privately developed rocket into  space next week - BusinessToday](https://akm-img-a-in.tosshub.com/businesstoday/images/story/202211/image_-_skyroot_prarambh_unveiling-sixteen_nine.jpg)
