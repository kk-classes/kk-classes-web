







  ## ✅ प्रधानमंत्री ग्रामीण आवास योजना में राजस्थान का कौनसा जिला पहले स्थान पर आया है- झुंझुनू
![Rajasthan District Map in Hindi HD](https://www.burningcompass.com/countries/india/maps/rajasthan-district-map-in-hindi-hd.jpg)

  ## ✅ हाल ही में ISRO ने गगणयान मिशन के क्रू मॉड्यूल की सुरक्षित लैंडिंग के लिए किसका सफल परिक्षण किया है- पैराशूट
![What is ISRO's IMAT Test for Gaganyaan Program?](https://st.adda247.com/https://s3-ap-south-1.amazonaws.com/adda247jobs-wp-assets-adda247/jobs/wp-content/uploads/sites/4/2022/11/21125859/ACA17AE6-C32A-4164-A281-9F686E656CD2.png)

  ## ✅ किस बॉलीवुड अभिनेता को 'सुपरस्टार ऑफ़ द डिकेड' अवार्ड से सम्मानित किया गया है- रणवीर सिंह
![Actor Ranveer Singh joins Indian startup story, invests in SUGAR Cosmetics  | Business Standard News](https://bsmedia.business-standard.com/_media/bs/img/article/2019-02/12/full/1549988381-1385.jpg)

  ## ✅ किस राज्य ने पूर्वोत्तर ओलंपिक खेलों में शीर्ष स्थान हासिल किया है- मणिपुर
![Northeast India - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/9/90/Northeast_india.png)

  ## ✅ ATP(एसोसिएशन ऑफ़ टेनिस प्रोफेशनल) रैंकिंग में नंबर 1 स्थान पाने वाले सबसे कम उम्र के खिलाडी कौन बने है- Carlos Alcaraz
![Rising star Carlos Alcaraz wins U.S. Open title and becomes youngest world  No. 1 | The Japan Times](https://cdn-japantimes.com/wp-content/uploads/2022/09/np_file_181646.jpeg)

  ## ✅ किस राज्य ने 9 दिवसीय अंतराष्ट्रीय फिल्म महोत्सव का उदघाटन किया है- गोवा
![Goa Maps](https://www.freeworldmaps.net/asia/india/goa/goa-map.jpg)

  ## ✅ किस फोरम ने गरीब देशों का समर्थन करने के लिए 'Loss And Damage' फंड लांच किया है- COP 27
![India to focus on climate finance, tech transfer at COP 27, delegation to  be led by Minister Bhupender Yadav - BusinessToday](https://akm-img-a-in.tosshub.com/businesstoday/images/story/202211/depositphotos_542465870-stock-photo-january-2022-brazil-photo-illustration-sixteen_nine.jpg?size=948:533)
