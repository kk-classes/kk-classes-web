





  ## ✅ किस देश ने COP-27 में 'इन ओवर लाइफटाइम' अभियान लांच किया है- भारत
![2022 United Nations Climate Change Conference - Wikipedia](https://upload.wikimedia.org/wikipedia/en/thumb/9/91/COP27_Logo.svg/1200px-COP27_Logo.svg.png)

  ## ✅ कौनसा देश 2024 में क्रिकेट वर्ल्ड कप अंडर-19 की मेजबानी करेगा- श्रीलंका
![Sri Lanka Map" Images – Browse 225 Stock Photos, Vectors, and Video | Adobe  Stock](https://t3.ftcdn.net/jpg/05/16/97/96/360_F_516979692_Riow3TU5kl9S9qMME6Z976pZEpnAziF6.jpg)

  ## ✅ हाल ही में केन्याई धावक रेंजू पर कितने साल का डोपिंग प्रतिबंध लगा है- 5 वर्ष
![Game over! Kenya's Kiprop slapped with hefty five-year ban for doping -  Capital Sports](https://www.capitalfm.co.ke/sports/files/2022/11/279336403_368325875232846_4315758093104396705_n-1024x576.jpg)

  ## ✅ एक ट्रिलियन डॉलर गंवाने वाली दुनिया की पहली सार्वजनिक सूचीबद्ध कंपनी कौन बनी है- अमेज़न
![Jeff Bezos Amazon Company Becomes the First Company in History to Lose $1  trillion Market Cap](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/11/16140212/L2aCmYvS-amazon1-1200x675-1.jpg)

  ## ✅ जलवायु परिवर्तन प्रदर्शन सूचकांक में भारत कौनसे स्थान पर रहा है- 8वे
![India Retains 10th Rank on Climate Change Performance Index 2022; Denmark  at 4th](https://affairscloud.com/assets/uploads/2021/11/India-retains-slot-in-top-10-on-Climate-Change-Performance-Index-CCPI-2022.jpg)
