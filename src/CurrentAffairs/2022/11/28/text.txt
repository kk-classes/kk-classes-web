





  ## ✅ नॉर्वे देश ने अफ़ग़ानिस्तान में मानवीय कार्यो के लिए कितने मिलियन क्रोनर दान की घोषणा की है- 220 मिलियन
![Kabul | History, Culture, Map, & Facts | Britannica](https://cdn.britannica.com/97/64597-050-37D333B9/Kabul-Afghanistan-locator-map-city.jpg)

  ## ✅ राष्रपति भवन 1 दिसंबर से सप्ताह में कितने दिन जनता के दर्शन के लिए खुलेगा- 5 दिन
![Mughal Gardens | Rashtrapati Bhavan](https://rashtrapatisachivalaya.gov.in/rbtour/sites/default/files/styles/circuit_image_slider_top/public/circuit/EX8_0771.jpg?itok=-vRd2ES5)

  ## ✅ किसने ग्रेटर नॉएडा के गौतमबुद्ध विश्वविद्यालय में आयोजित 'यूनेस्को इंडिया-अफ्रीका है थाकान 2022' का शुभारंभ किया है- योगी आदित्यनाथ
![Yogi Adityanath to inaugurate Uttar Pradesh BJP executive meeting | Deccan  Herald](https://www.deccanherald.com/sites/dh/files/styles/article_detail/public/articleimages/2022/05/29/yogi-adityanath-pti-1113452-1653805854.jpg?itok=6dMPqoc6)

  ## ✅ 'कर्मयोगी प्रारंभ' किस वर्ग के लोगो के लिए तैयार किया गया पाठ्यक्रम है- सरकारी कर्मचारी
![PM Launches Karmayogi Prarambh Module: An initiative Under Rojgar Mela](https://img.jagranjosh.com/images/2022/November/22112022/PM-Launches-Karmayogi-Prarambh-Module-min.jpg)

  ## ✅ संगाई महोत्सव 2022 किस राज्य में मनाया जाने वाला सांस्कृतिक उत्सव है- मणिपुर
![Sangai Festival – It's only getting bigger and better! - MyGov Blogs](https://blog.mygov.in/wp-content/uploads/2019/11/Inner-Image-6.jpg)
