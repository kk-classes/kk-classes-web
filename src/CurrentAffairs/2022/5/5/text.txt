










  ## ✅ हाल ही में इंग्लैंड देश में दुनिया का पहला टैक्सी फ्लाइंग एयरपोर्ट 'अर्बन एयर वन वर्टीपोर्ट' बनाया गया है
![Autonomous Flying Taxis One Step Closer to Reality – IoT World Today](https://www.iotworldtoday.com/files/2022/03/GettyImages-1236491842-877x432.jpg)

  ## ✅ हाल ही में अमेरिका की खुफ़िआ एजेंसी 'सेंट्रल इंटेलिजेंस एजेंसी' ने भारतीय मूल के नंद मूलचंदानी को अपना पहला मुख्य प्रौद्योगीकी अधिकारी नियुक्त किया है
![Indian-Origin Nand Mulchandani Appointed as CTO by CIA](https://www.pgurus.com/wp-content/uploads/2022/05/Nand-Mulchandani.jpg)

  ## ✅ हाल ही में विनय मोहन कवातरा भारत के नए विदेश सचिव बने है
![Vinay Mohan Kwatra, India's ambassador to Nepal, named new foreign  secretary | Latest News India - Hindustan Times](https://images.hindustantimes.com/img/2022/04/04/550x309/vinay_1649076206533_1649076220690.jpg)

  ## ✅ हाल ही में केंद्र सरकार ने राजस्थान में स्थित 'मिया का बाड़ा' रेलवे स्टेशन का नाम बदलकर महेश नगर रेलवे स्टेशन रख दिया है
![Rajasthan village Miyan Ka Bara renamed as Mahesh Nagar - India News](https://akm-img-a-in.tosshub.com/indiatoday/images/story/201808/Rajasthan_Village_ANI.jpeg?Hi8uUJWc58H0sb4hzxinPlCY8J9jppoK)

  ## ✅ हाल ही में महाराष्ट्र राज्य सरकार ने जेनेटिक संसाधनों के संरक्षण के लिए भारत के पहले जीन बैंक परियोजना को मंजूरी दी है
![Maharashtra Map, Districts in Maharashtra | India world map, Map, Geography  map](https://i.pinimg.com/originals/ba/2d/a2/ba2da274afe362c68c55a26d1185ebeb.jpg)

  ## ✅ हाल ही में ब्राज़ील देश की लैंडस्कैप गार्डन 'सीटियों बर्ल मार्क्स' को यूनेस्को की विश्व धरोहर स्थल का दर्जा प्राप्त हुआ है
![Garden landscaping ideas – 10 steps to landscape a garden from scratch |  Real Homes](https://cdn.mos.cms.futurecdn.net/deqfGXD9AMbBhXf75irkdg-768-80.jpg)

  ## ✅ हाल ही में पूर्णिया बिहार में भारत के पहले मोटे आनाज से संचालित होने वाले 'ग्रीन फील्ड ग्रेन बेस्ड एथेनॉल प्लांट' का उदघाटन किया गया है
![Bihar Map, Bihar Districts | Map, Bihar, Ancient india map](https://i.pinimg.com/736x/ce/74/38/ce74384ce478797b7ae3cc3bf1a3b264.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/cR3bZdaugCrsnNWY8
