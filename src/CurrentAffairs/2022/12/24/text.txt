












  ## ✅ दिसंबर 2022 में खेलों इंडिया महिला टूर्नामेंट में किस खेल का पहली बार आयोजन किया गया है- ट्रैक साइकिलिंग
![How to get started at track cycling - BikeRadar](https://images.immediate.co.uk/production/volatile/sites/21/2016/08/RussellBurton_20180210_2DX4544-99b72e5.jpg?quality=90&resize=620%2C413)

  ## ✅ फ्रांस के किस फुटबॉलर ने अपने 35वे जन्मदिन पर अंतराष्ट्रीय फुटबॉल से संन्यास की घोषणा की है- Karim Benzema
![Ballon D'or winner Benzema 'ends' France career after FIFA World Cup 2022  final | Football News - Hindustan Times](https://images.hindustantimes.com/img/2022/12/19/550x309/Soccer-World-Cup-Benzema-s-Redemption--1_167146171_1671461724219_1671461724343_1671461724343.jpg)

  ## ✅ भारतीय नौसेना में शामिल हुई पांचवी स्कॉर्पियन पनडुब्बी का नाम क्या है- INS Vagir
![INS Vagir Launch Video | 5th Scorpene class Submarine - YouTube](https://i.ytimg.com/vi/0NzEtf2sYoI/maxresdefault.jpg)

  ## ✅ कौनसा देश 'संयुक्त राष्ट्र जैव विविधता सम्मलेन- COP15' का मेजबान है- कनाडा
![Canada Map | Infoplease](https://i.infopls.com/images/mapcanada.gif)

  ## ✅ हाल ही में किसने 'द लाइट वी कैरी' पुस्तक लिखी है- Michelle Obama
![Michelle Obama speaks about new book, 'The Light We Carry' - Good Morning  America](https://s.abcnews.com/images/GMA/221112_gma_obama_hpMain_16x9_1600.jpg)
