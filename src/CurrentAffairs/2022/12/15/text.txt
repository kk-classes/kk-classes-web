







  ## ✅ प्रकृति को बचाने के लिए हाल ही में कहा पर COP-15 शुरू हुआ है- कनाडा
![Montreal to host delayed Cop15 summit to halt 'alarming' global  biodiversity loss | Environment | The Guardian](https://i.guim.co.uk/img/media/497b5f917ea316404a437d196e9329753c18348f/0_374_5184_3110/master/5184.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=1975e188d576326b1864c1491934d7bb)

  ## ✅ किसने वर्ल्ड एथलीट ऑफ़ द ईयर 2022 का पुरस्कार जीता है- McLaughin Levron और Mondo Duplantis
![Sydney McLaughlin and Armand Duplantis have been honored with World  Athletics Awards 2022.](https://ichef.bbci.co.uk/news/1024/branded_sport/16AD9/production/_127898829_63867904.jpg)

  ## ✅ किस देश में जीवाणु संक्रमण Strap A से बच्चों की मृत्यु दर्ज की गई है- UK
![United Kingdom Map | England, Scotland, Northern Ireland, Wales](https://geology.com/world/united-kingdom-map.gif)

  ## ✅ चुनाव आयोग ने TRS का नाम बदलकर क्या रख दिया है- BRS
![Transformation of TRS to BRS questioned, no activity in national politics](https://cdn.siasat.com/wp-content/uploads/2022/10/TRS-TO-BRS.jpg)

  ## ✅ भारत सरकार ने वर्ष 2023 तक कुल कितने खेलो इंडिया केंद्र खोलने का लक्ष्य रखा है- 1000
![Khelo India Youth Games to be held from June 4 to 13](https://thebridge.in/h-upload/2022/04/26/26344-feature-image-khelo-india-.webp)

  ## ✅ RBI ने किस देश के 'मौद्रिक प्राधिकरण' के साथ मुद्रा अदला बदली के समझौते पर हस्ताक्षर किया है- मालदीव्स
![Perspectives on India-China competition in Maldives – Asia Times](https://i0.wp.com/asiatimes.com/wp-content/uploads/2018/09/Maldives-China-India-Political-Map-Facebook.jpg?fit=1200%2C887&ssl=1)
