













  ## ✅ हाल ही में Dr Ian Fry को संयुक्त राष्ट्र मानवाधिकार परिषद ने मानव अधिकाओ और जलवायु परिवर्तन के लिए दुनिया का पहला स्वतंत्र विशेषज्ञ नियुक्त किया है
![Viktor Orban wins Fourth Term as Prime Minister of Hungary](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/04/05073004/ian-fry-tuvalu.jpg)

  ## ✅ हाल ही में Railway Protection Force ने अवैध टिकटिंग के खिलाफ 'ऑपरेशन उपलब्ध' नामक अभियान चलाया है
![Operation Uplabdh: RPF arrest 341 IRCTC agents involved in ticket touting](https://ptcnews-wp.s3.ap-south-1.amazonaws.com/wp-content/uploads/2022/04/341-IRCTC-agents-held-3.jpg)

  ## ✅ हाल ही में छत्तीसगढ़ राज्य भारत में सबसे कम बेरोज़गारी दर वाले राज्यों में शीर्ष स्थान पर रहा है
![Chhattisgarh Map | Chhattisgarh, India world map, Map](https://i.pinimg.com/736x/02/e6/c5/02e6c5b13686be68aaf117d121a2b24a--notification-cg.jpg)

  ## ✅ हाल ही में देविका रंगाचारी ने 'क्वीन ऑफ़ फायर' नामक पुस्तक लिखी है
![A new book titled “Queen of Fire” authored by Devika Rangachari | Sakshi  Education](https://education.sakshi.com/sites/default/files/images/2022/04/06/queenoffire-1649250736.jpg)

  ## ✅ हाल ही में Viktor Orban हंगरी देश के नए प्रधानमंत्री बने है
![Hungary's Viktor Orban criticizes Ukraine's Zelenskyy in election speech](https://image.cnbcfm.com/api/v1/image/107041147-gettyimages-1239726965-AFP_327L2Q2.jpeg?v=1649071349&w=929&h=523&ffmt=webp)

  ## ✅ हाल ही में उत्तर प्रदेश के मुख्यमंत्री योगी आदित्यनाथ ने श्रावस्ती जिले से 'स्कूल चलो अभियान' की शुरुआत की है
![BSA noticed 50 officers to report on down registration in primary school |  यूपी: 'स्कूल चलो अभियान' की रफ्तार सुस्त, 50 जिलों के बीएसए से रिपोर्ट तलब](https://hindi.cdn.zeenews.com/hindi/sites/default/files/2018/08/04/267460-school-chale-abhiyan.jpg)

  ## ✅ हाल ही में एम.के स्टालिन ने आपातकाल के दौरान जनता के लिए 'कावल उथ्वी' नामक एप को लांच किया है
![Claiming to be third force in TN with 10% score: CM Stalin's jibe at BJP |  The News Minute](https://www.thenewsminute.com/sites/default/files/styles/news_detail/public/MKStalin_030422_1200x800_Facebook.jpg?itok=dnkrzchu)

  ## ✅ हाल ही में ग्रामीण छेत्र और महिलाओं के विकास के लिए घरेलु ई-कॉमर्स कंपनी फ्लिपकार्ट ने Flipkart Foundation नाम से एक नया प्लेटफार्म शुरू किया है
![Flipkart expands its grocery services to Tamil Nadu and Kerala](https://images.livemint.com/img/2021/08/13/1600x900/2021-07-12T072409Z_1235904355_RC2VIO96NVSN_RTRMADP_3_WALMART-INVESTMENT-FLIPKART_1628317825240_1628856903274.JPG)




  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/dZP1MpruLEm2UgB3A
