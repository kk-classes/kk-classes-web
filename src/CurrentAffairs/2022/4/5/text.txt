











  ## ✅ हाल ही में मणिपुर राज्य सरकार ने स्पोर्ट्स डिजिटल एक्सपीरियंस सेंटर शुरू करने के लिए सैमसंग कंपनी के साथ समझौता किया है
![Manipur govt signs MoU with Samsung and Abhitech to set up sports digital  experience centre in Imphal, Government News, ET Government](https://etimg.etb2bimg.com/photo/90556389.cms)

  ## ✅ हाल ही में जारी वित्त वर्ष 2021-22 के अनुसार उत्तर प्रदेश राज्य सबसे अधिक सब्जी उत्पादन वाला राज्य बन गया है
![Vegetables](https://www.healthyeating.org/images/default-source/home-0.0/nutrition-topics-2.0/general-nutrition-wellness/2-2-2-2foodgroups_vegetables_detailfeature.jpg?sfvrsn=226f1bc7_6)

  ## ✅ हाल ही में एन.वी रमन्ना ने अदालती आदेशों को तेज़ी से प्रसारित करने के लिए 'फास्टर' नाम से एक सॉफ्टवेयर लांच किया है
![CJI NV Ramana launched FASTER to Securely and Electronically transmit Bail  and other orders](https://affairscloud.com/assets/uploads/2021/07/electronically-transmit-bail-and-other-orders.jpg)

  ## ✅ हाल ही में Dwayne Bravo IPL के इतिहास में सबसे ज्यादा विकेट लेने वाले गेंदबाज बन गए है
![Dwayne Bravo to retire from international cricket after T20 World Cup 2021  | Cricket News – India TV](https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2021/11/gettyimages-1349179307-1-1636095348.jpg)

  ## ✅ हाल ही में मीना नय्यर द्वारा लिखी पुस्तक 'द टाइगर ऑफ़ द्रास' को प्रकाशित किया गया है
![Buy Tiger of Drass: Capt. Anuj Nayyar, 23, Kargil Hero Book Online at Low  Prices in India | Tiger of Drass: Capt. Anuj Nayyar, 23, Kargil Hero  Reviews & Ratings - Amazon.in](https://images-na.ssl-images-amazon.com/images/I/818n-GQoxNL.jpg)

  ## ✅ हाल ही में माइक्रोसॉफ्ट ने स्टार्टअप फॉउडर्स हब नामक प्लेटफार्म को लांच किया है
![Microsoft launches new Microsoft for Startups Founders Hub platform in  India - The Hindu BusinessLine](https://www.thehindubusinessline.com/incoming/uhwzwp/article65277290.ece/alternates/LANDSCAPE_1200/microsoft.jpg)

  ## ✅ हाल ही में महाराष्ट्र राज्य जेल में बंद कैदियों के लिए व्यक्तिगत ऋण देने की योजना शुरू करने वाला भारत का पहला राज्य बन गया है
![Maharashtra Map, Districts in Maharashtra | India world map, Map, Geography  map](https://i.pinimg.com/originals/ba/2d/a2/ba2da274afe362c68c55a26d1185ebeb.jpg)






  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/dZP1MpruLEm2UgB3A
