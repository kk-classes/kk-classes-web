



  ## ✅ हाल ही में रस्किन बांड ने 'ए लिटिल बुक ऑफ़ इंडिया' नामक पुस्तक लिखा है
![A Little Book of India: Celebrating 75 years of Independence&#39; authored by  Ruskin Bond](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/01/29091504/89155626.jpg)

  ## ✅ हाल ही में असम राज्य ने रतन टाटा को अपना सर्वोच्च नागरिक सम्मान प्रदान किया है
![Ratan N Tata](https://www.tatatrusts.org/Upload/Images/masthead/ratan-tata-img.jpg)

  ## ✅ हाल ही में अंतराष्ट्रीय क्रिकेट परिषद ने बाबर आज़म को 2021 में ICC मेंस वनडे क्रिकेटर ऑफ़ द ईयर चुना है
![Pakistan captain Babar Azam wins ICC Men&#39;s ODI cricketer of 2021 award |  Cricket News | Zee News](https://english.cdn.zeenews.com/sites/default/files/2022/01/24/1008312-babar-azam.jpg)

  ## ✅ हाल ही में दिल्ली सरकार ने यूरोपीय शहरों की तर्ज पर सड़कों का सौंदरगीकरण परियोजना शुरू किया है
![Delhi Outline Map | Map, Delhi map, Geography map](https://i.pinimg.com/originals/4c/33/b8/4c33b82cdf5f1d747ee3e6ae8984f36d.jpg)






  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/fC8ae8ivea4gp7zq8
