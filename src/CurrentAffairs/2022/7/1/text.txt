














  ## ✅ केरल राज्य सरकार ने सरकारी कर्मचारियों और पेंशनभोगियों के लिए 'MEDISEP' नामक चिकित्सा बीमा योजना शुरू की है
![Kerala govt nod for MEDISEP, new insurance scheme to commence from New Year  | Onmanorama](https://img.onmanorama.com/content/dam/mm/en/kerala/top-news/images/2021/12/23/health-insurance.jpg)

  ## ✅ विजय अमृतराज को अंतराष्ट्रीय टेनिस हॉल ऑफ़ फेम और अंतराष्ट्रीय टेनिस महासंघ ने 'गोल्डन अचीवमेंट' पुरस्कार 2021 से सम्मानित किया है
![Vijay Amritraj - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/7/7d/The_former_Tennis_player%2C_Shri_Vijay_Amritraj_calling_on_the_Minister_of_State_for_Youth_Affairs_and_Sports_%28IC%29%2C_Water_Resources%2C_River_Development_and_Ganga_Rejuvenation%2C_Shri_Vijay_Goel%2C_in_New_Delhi_on_April_11%2C_2017_%28cropped%29.jpg)

  ## ✅ एस.एस मुंद्रा को BSE का नया अध्यक्ष नियुक्त किया गया है
![S.S. Mundra: RBI must pronounce its views, but also be ready to listen |  Mint](https://images.livemint.com/rf/Image-621x414/LiveMint/Period2/2018/12/14/Photos/Processed/ss-mundra-kjkB--621x414@LiveMint.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/2aGLUCuqX1GAYx5D9
