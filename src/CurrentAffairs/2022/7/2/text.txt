

















  ## ✅ हाल ही में नितिन गुप्ता केंद्रीय प्रत्यक्ष कर बोर्ड(CBDT) के नए अध्यक्ष बने है
![Nitin Gupta, Founder and CEO of Uni — Launching a Pay-Later Product in  India | by Anirudh Singh | Wharton FinTech | Medium](https://miro.medium.com/max/1072/1*--q3yo9O5Y0s7FZaw5gJlw.jpeg)

  ## ✅ हाल ही में ओडिशा राज्य की सार्वजनिक परिवहन सेवा 'मो बस' को प्रतिष्ठित संयुक्त राष्ट्र लोक सेवा पुरस्कार 2022 से सम्मानित किया गया है
![Odisha Map - Patra Tours and Travels](https://www.patratravels.com/images/about/map-of-odisha.jpg)

  ## ✅ हाल ही में लिस्बन(पुर्तगाल) में समुंद्री प्रदुषण से निपटने के लिए संयुक्त राष्ट्र महासागर सम्मलेन 2022 का आयोजन किया गया है
![Portugal Political Map with capital Lisbon, national borders, most  important cities, rivers and lakes. English labeling Stock Photo - Alamy](https://c8.alamy.com/comp/E1YX7A/portugal-political-map-with-capital-lisbon-national-borders-most-important-E1YX7A.jpg)

  ## ✅ हाल ही में मध्य प्रदेश राज्य ने रणजी ट्रॉफी 2021-22 का खिताब जीता है
![Ranji Trophy 2022: Madhya Pradesh beats Mumbai by six wickets](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/06/27075416/2022_6largeimg_316437251-scaled.jpg)

  ## ✅ हाल ही में रोनाल्डो सिंह एशिआई चैंपियनशिप में रजत पदक जितने वाले पहले भारतीय साइकिलिस्ट बन गए है
![Cyclist Ronaldo Singh creates history, becomes first Indian to win silver  in Asian Championship -](https://newsonair.com/wp-content/uploads/2022/06/17-1068x713.jpg)

  ## ✅ हाल ही में ICICI बैंक ने छात्र पारिस्थितिक तंत्र के लिए 'कैंपस पॉवर' लांच किया है
![ICICI Bank Launches Money2World Platform With New Digital Facilities - BW  Businessworld](https://static.businessworld.in/article/article_extra_large_image/1494845216_ZCUVVt_icici-reu470.jpg)

  ## ✅ हाल ही में अनिल खन्ना को भारतीय ओलंपिक संघ के कार्यवाहक अध्यक्ष के रूप में नियुक्त किया गया है
![Indian Olympic Association: Anil Khanna named as the acting President of IOA](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/06/27081416/anil-khanna-latest-main.jpg)

  ## ✅ हाल ही में अमेरिका देश की सुप्रीम कोर्ट ने 50 साल पुराने कानून को पलटने के लिए 'गर्भपात के संवैधानिक अधिकार' को समाप्त कर दिया है
![United States Map with Capitals, US Map, US States and Capitals Map](https://www.burningcompass.com/countries/united-states/states/maps/united-states-map-with-capitals.jpg)





  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/2aGLUCuqX1GAYx5D9
