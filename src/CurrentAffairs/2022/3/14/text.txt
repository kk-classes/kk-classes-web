














  ## ✅ हाल ही में बी चंद्रशेखर भारतीय वायु सेना अकादमी(IAFA) के नए कमांडेंट बने है
![Daily News Prime](https://i0.wp.com/img.jagranjosh.com/images/2022/March/932022/Air_force_Academy.jpg?w=780&ssl=1)

  ## ✅ हाल ही में हरयाणा राज्य के मुख्यमंत्री मनोहरलाल खट्टर ने महिला उद्यमियों को सहायता प्रदान करने के लिए 'मातृशक्ति उद्यमिता योजना' की शुरुआत की है
![Haryana CM's video kicks up a controversy - The Hindu](https://www.thehindu.com/news/national/other-states/lpoanm/article36810001.ece/alternates/LANDSCAPE_615/DE20-KHATTAR)

  ## ✅ हाल ही में बोध गया बिहार में भगवन बुद्ध की शयन मुद्रा में दुनिया की सबसे लंबी प्रतिमा बनाई गई है
![Explained: The Reclining Buddha and his various other depictions in art |  Explained News,The Indian Express](https://images.indianexpress.com/2021/05/buddha-new.jpg)

  ## ✅ हाल ही में वैश्विक फार्मा प्रमुख लुपिन्स लिमिटेड ने अपनी शक्ति पहल के लिए मैरी कॉम को अपना ब्रांड एम्बेसडर बनाया है
![Mary Kom - Biography, Achievements, Personal Life, Images](https://mybestguide.com/wp-content/uploads/2020/11/Mary-Kom.jpeg)







  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/6xwPSwYQex9KutnB7
