
















  ## ✅ हाल ही में गिरिराज सिंह ने महात्मा गाँधी राष्ट्रीय ग्रामीण रोगजार गारंटी अधिनियम के लिए 'लोकपाल एप' लांच किया है
![केंद्रीय मंत्री श्री गिरिराज सिंह ने महात्मा गांधी नरेगा के लिए लोकपाल ऐप  लॉन्च किया](https://static.pib.gov.in/WriteReadData/userfiles/image/image001230R.jpg)

  ## ✅ हाल ही में पनडुब्बी रोधी युद्धक विमान P-8I को भारतीय नौसेना में शामिल किया गया है, इसका निर्माण Boeing Aerospace(USA) ने किया है
![Everything About P8-I, The Submarine Hunter| Indian Should buy 30 more? -  YouTube](https://i.ytimg.com/vi/aCoIQQvDmw8/maxresdefault.jpg)

  ## ✅ हाल ही में वंदे भारतम के लिए एक सिगनेचर गाने की धुन को जारी किया गया है, इस गाने को Ricky Cage और Bikram Ghosh ने लिखा है
![Vande Bharat - Vande Matram Song | New Patriotic Song 2022 | Desh Bhakti  Song | Veena Music - YouTube](https://i.ytimg.com/vi/Sgu1VBfFkkY/maxresdefault.jpg)

  ## ✅ हाल ही में अनिरुद्ध सूरी ने 'The Great Tech Game' नामक पुस्तक लिखा है
![HarperCollinsPublishers is proud to announce The Great Tech Game: Shaping  Geopolitics and the Destinies of Nations by Anirudh Suri - HarperCollins  Publishers India](https://harpercollins.co.in/wp-content/uploads/2022/01/the-great-tech-game-blog-size-19-1280x720.jpg)

  ## ✅ हाल ही में मीराबाई चानु ने सिंगापूर वेटलिफ्टिंग इंटरनेशनल में स्वर्ण पदक जीता है
![टोक्यो ओलंपिक में मीराबाई चनू ने जीता सिल्वर, पीएम मोदी समेत तमाम नेताओं ने  दी बधाई - Tokyo Olympic 2020 Meerabai Chanu won silver medal in  Weightlifting PM Modi and congratulates her](https://akm-img-a-in.tosshub.com/aajtak/images/story/202107/meerabai_chanu_0-sixteen_nine.jpg?size=948:533)

  ## ✅ हाल ही में ऋषभ पंत डिश टीवी इंडिया के ब्रांड एम्बेसडर बने है
![Rishabh Pant Brand Ambassador: Dish TV&#39;s ropes Rishabh Pant](https://st.adda247.com/https://wpassets.adda247.com/wp-content/uploads/multisite/sites/5/2022/02/25085021/Rishabh-Pant-joins-D2H-as-its-Brand-Ambassador.jpg)





  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/6xwPSwYQex9KutnB7
