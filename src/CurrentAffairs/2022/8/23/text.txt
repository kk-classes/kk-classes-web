





23

  ## ✅ हाल ही में नई दिल्ली में एशियन वस्त्र सम्मलेन 2022 आयोजित किया गया है
![Central Asian Leaders Won t Make It To New Delhi For R Day Modi To Host  Virtual Summit Instead - BW Businessworld](https://static.businessworld.in/article/article_extra_large_image/1642583973_OhAuA8_foreign_ministers_Govt.jpg)

  ## ✅ हाल ही में उत्तराखंड राज्य सरकार ने प्राथमिक स्कूलों में बच्चों में स्वछता के प्रति जागरूकता पैदा करने के लिए 'डिटोल स्कूल स्वछता शिक्षा कार्यक्रम' शुरू किया है
![Dettol School Hygiene Education Programme In 8 States Has Reduced Diarrhoea  Cases By 14.6% | Reckitt's Commitment For A Better Future](https://w.ndtvimg.com/sites/3/2021/10/14162524/dettol-school-education-programme.jpg)

  ## ✅ हाल ही में समुंद्री निगरानी में मदद के लिए भारत ने श्रीलंका देश को पहला 'डोर्नियर' नामक समुंद्री निगरानी विमान सौंपा है
![File:Do228NG - RIAT 2012 (18649688613).jpg - Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/0/0b/Do228NG_-_RIAT_2012_%2818649688613%29.jpg)

  ## ✅ हाल ही में दादर नगर हवेली दमन दिउ 'जल जीवन मिशन' के तहत भारत के पहला 'हर घर जल' प्रमाणित केंद्र शाषित प्रदेश बन गया है
![Dadra and Nagar Haveli and Daman and Diu Merger Bill, 2019](https://img.jagranjosh.com/imported/images/E/GK/dadra-and-nagar-haveli-and-daman-and-diu-map.jpg)

  ## ✅ हाल ही में गोवा 'जल जीवन मिशन' के तहत भारत के पहला 'हर घर जल' प्रमाणित केंद्र शाषित प्रदेश बन गया है
![Goa becomes first 'Har Ghar Jal' state by providing tap water connections  in rural areas](https://pbs.twimg.com/media/Ej5eeEgVcAENwF5?format=jpg&name=small)

  ## ✅ हाल ही में अरुणाचल प्रदेश राज्य ने भारत में दवाई की सप्लाई के लिए 'आसमान से दवा' नामक ड्रोन सेवा शुरू किया है
![Air care: Study looks at using drones to deliver medicine and diagnostics ←  Research @ Texas A&M | Inform, Inspire, Amaze](https://research.tamu.edu/wp-content/uploads/2018/06/drone-meds-feature.jpg)

  ## ✅ हाल ही में मुंबई में भारत की पहली 'इलेक्ट्रॉनिक व्हीकल 22' नामक डबल डेकर इलेक्ट्रॉनिक वातानुकूल बस सेवा शुरू की गई है
![London's iconic double-decker bus given an electric makeover - Industry  Europe](https://industryeurope.com/downloads/7672/download/Equipmake%20Jewel%20E%20electric%20bus.png?cb=08968a66305d013ebdc6feb002110627)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/4p4TdFnb3FJr9RFv7
