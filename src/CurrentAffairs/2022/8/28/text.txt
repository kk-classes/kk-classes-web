













  ## ✅ हाल ही में केंद्रीय विदेश मंत्री एस जयशंकर ने 21 अगस्त 2022 को भारत के साथ राजनयिक संबंधो की 60वी वर्षगांठ पर पैरागुवे देश में भारतीय दूतावास का उदघाटन किया है
![Relationship Not Normal, Can't Be...": S Jaishankar On India-China Ties](https://c.ndtvimg.com/2022-04/58h2h1go_s-jaishankar-pti_625x300_13_April_22.jpg)

  ## ✅ हाल ही में भारतीय चिकित्सक सुंदर चरी ने प्रतिष्ठित 'फेल्लो ऑफ़ द अमेरिकन एकेडमी ऑफ़ न्यूरोलॉजी पुरस्कार 2022' जीता है
![Who is the Second Most Dangerous Politician in America? - Shepherd Express](https://shepherdexpress.com/downloads/29416/download/paulryan.jpg.jpe?cb=64c3ca535d9f20df798a66bad0a14a99&w=1000)

  ## ✅ हाल ही में केंद्रीय वित्त मंत्री निर्मला सीतारमण ने नागालैंड राज्य में 'मुख्यमंत्री शूक्ष्म वित्त योजना' को शुरू किया है
![The importance of being Nirmala Sitharaman — only BJP minister facing media  line of fire](https://static.theprint.in/wp-content/uploads/2022/02/2022_2img01_Feb_2022_PTI02_01_2022_000192B.jpg?compress=true&quality=80&w=376&dpr=2.6)

  ## ✅ हाल ही में फ्यू रिसर्च सेंटर द्वारा जारी 'Indias Sex Ratio At Birth Begins To Normalize' रिपोर्ट के अनुसार वर्ष 2019-20 में जन्म के समय भारत का लिंगानुपात 108 रहा है
![Girls make a comeback: India's skewed sex ratio at birth begins to normalize  - Indica news](https://indicanews.com/wp-content/uploads/2022/08/Children-India.jpg)

  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/4p4TdFnb3FJr9RFv7
