










  ## ✅ हाल ही में Google Maps कंपनी ने ट्रैफिक रोड पर चल रहे काम की जानकारी प्रदान करने के लिए 'स्ट्रीट व्यू सेवा' की शुरुआत की है
![Here's how you can pin location in Google Maps: Steps to follow](https://st1.bgr.in/wp-content/uploads/2022/04/Google-Maps.jpg)

  ## ✅ हाल ही में Anshsta Singh बर्मिंघम राष्ट्रमंडल खेल 2022 में भाग लेने वाली भारत की सबसे कम उम्र की महिला एथलिट बन गई है
![Meet 14-year old squash player Anahat Singh, the youngest member of India's  Birmingham Commonwealth Games 2022 contingent | Sports News,The Indian  Express](https://images.indianexpress.com/2022/07/anahat-singh.jpg)

  ## ✅ हाल ही में Manisha Ropeta पाकिस्तान देश की पहली महिला हिंदू उप पुलिस अधीक्षक(DSP) बनी है
![Manisha Ropeta, Pakistan's First Hindu Woman Senior Cop](https://im.indiatimes.in/content/2022/Jul/manisha-ropeta_62e3d178446cb.jpg?w=1100&h=535&cc=1)

  ## ✅ हाल ही में ओडिशा राज्य के मुख्यमंत्री ने 'लॉक डाउन लिरिक्स' नामक पुस्तक का विमोचन किया है
![Odisha CM Releases Book 'Lockdown Lyrics', A Collection of Poems -  Pragativadi](https://pragativadi.com/wp-content/uploads/2022/07/images-1-9-680x430.jpeg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/4p4TdFnb3FJr9RFv7
