






  ## ✅ हाल ही में पुरुषोत्तम रूपला ने भारतीय मतस्य पालन को बढ़ावा देने के लिए 'एक्वा बाजार' नामक एप को लांच किया है
![Parshottam Rupala inaugurates APP and Website at NDDB - The Economic Times](https://m.economictimes.com/thumb/msid-85718174,width-1200,height-900,resizemode-4,imgsize-51588/parshottam-rupala.jpg)

  ## ✅ हाल ही में हैलिफैक्स(कनाडा) में 65वा राष्ट्रमंडल संसदीय सम्मलेन 2022 आयोजित किया गया है
![Canada Map | Detailed Maps of Canada](https://ontheworldmap.com/canada/map-of-canada.jpg)

  ## ✅ हाल ही में मनीषा कल्याण यूरोप में आयोजित 'UEFA महिला चैंपियनशिप लीग 2022' खेलने वाली भारत की पहली फुटबॉल खिलाडी बन गई है
![WATCH: India's Manisha Kalyan scores a historic goal against Brazil](https://thebridge.in/h-upload/2021/07/21/11065-manisha-kalyan.webp)

  ## ✅ हाल ही में पुणे में भारत की पहली स्वदेशी रूप से विकसित 'हाइड्रोजन ईंधन सेल बस' का शुभारंभ किया गया है
![Sarawak government rejects SPAD's proposal for diesel buses, will focus on  electric, hydrogen buses - paultan.org](https://paultan.org/image/2017/12/Putra-NEDO-EV-Bus-4-e1512457269157.png)

  ## ✅ हाल ही में चंडीगढ़ हवाई अड्ढे का नाम बदलकर 'शहीद भगत सिंह अंतराष्ट्रीय हवाई अद्धा' रख दिया गया है
![Bhagat Singh Biography: Birth, Age, Education, Jailterm, Execution, and  More About Shaheed-e-Azam | Shaheed Diwas](https://img.jagranjosh.com/images/2021/September/2792021/Bhagat-singh-biography-jayanti-birth-anniversary-death-facts.jpg)

  ## ✅ हाल ही में केंद्र सरकार द्वारा बिहार राज्य के 'मिथिला मखाना' को GI टैग प्रदान किया गया है
![Madhubani के मखाना को मिला GI Tag - YouTube](https://i.ytimg.com/vi/XDqi4CG4pV4/hqdefault.jpg)

  ## ✅ हाल ही में छत्तीसगढ़ राज्य सरकार ने ग्रामीण गरीब परिवारों की आजीविका में सुधार करने के लिए '300 ग्रामीण आजीविका पार्क' विकसित किया गया है
![Chhattisgarh Map | Chhattisgarh, India world map, Map](https://i.pinimg.com/originals/02/e6/c5/02e6c5b13686be68aaf117d121a2b24a.jpg)

  ## ✅ हाल ही में अनिम पंघल बुल्गारिया के सोफिया में आयोजित अंडर-20 विश्व कुश्ती चैम्पियनशिप 2022 में 'स्वर्ण पदक' जितने वाली पहली भारतीय महिला बन गई है
![Amit Panghal (@Boxerpanghal) / Twitter](https://pbs.twimg.com/profile_images/1035933879830564866/Vqb-DdT8_400x400.jpg)


  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/4p4TdFnb3FJr9RFv7
