










  ## ✅ हाल ही में तमिलनाडु राज्य में लुप्तप्राय 'सिलेंडर लॉरिस' के लिए भारत का पहला अभ्यारण स्थापित किया गया है
![Slender loris - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Sri_Lankan_Slender_Loris_1.jpg/640px-Sri_Lankan_Slender_Loris_1.jpg)

  ## ✅ हाल ही में World Wild Fund For Nation संस्थान ने Living Planet Report 2022' जारी किया है
![प्रकृति संरक्षण हेतु विश्वव्यापी कोष Worldwide Fund for Nature – WWF |  Vivace Panorama](http://www.vivacepanorama.com/wp-content/uploads/2015/08/Worldwide-Fund-for-Nature.jpg)

  ## ✅ हाल ही में राजस्थान राज्य के विभिन्न जिलों में पेयजल की गुडवत्ता जांच के लिए 250 नई प्रयोगशालाए स्थापित की गई है
![Download Rajasthan Map in HD quality! 2022 - UPSC Colorfull notes](https://egee5a6fk74.exactdn.com/wp-content/uploads/2022/07/rajasthan-map-1024x934.jpg?strip=all&lossy=1&ssl=1)

  ## ✅ हाल ही में पूर्वी नैसेना ने आंध्र प्रदेश राज्य में 'Prasthan' नामक अभ्यास शुरू किया है
![Exercise Prasthan - GKToday](https://i0.wp.com/www.gktoday.in/wp-content/uploads/2021/11/exercise-prasthan.png?fit=1200%2C675&ssl=1)

  ## ✅ हाल ही में श्रीलंका देश के लगभग 500 डॉक्टरों ने जारी आर्थिक संकट के बीच पलायन किया है
![Sri Lanka Map" Images – Browse 214 Stock Photos, Vectors, and Video | Adobe  Stock](https://t3.ftcdn.net/jpg/05/16/97/96/360_F_516979692_Riow3TU5kl9S9qMME6Z976pZEpnAziF6.jpg)

  ## ✅ हाल ही में राष्ट्रपति द्रोपदी मुर्मू ने IIT गुवाहाटी में सुपर कंप्यूटर सुविधा का उदघाटन किया है
![IIT-Guwahati to be given land by Assam government for setting up medical  college](https://i0.wp.com/nenow.in/wp-content/uploads/2021/04/iit-g.jpg?fit=1200%2C594&ssl=1)

  ## ✅ हाल ही में उत्तर प्रदेश राज्य के मंत्रिमंडन ने नई 'इलेक्ट्रॉनिक वाहन निति' को मंजूरी दी है
![Uttar Pradesh Electric Vehicle Policy (Draft) 2022-27 - E-Vehicleinfo](https://e-vehicleinfo.com/wp-content/uploads/2022/09/Uttar-Pradesh-Electric-Vehicle-Policy-Draft-2022-27.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/FwtYv8ZHJgvtR7yw7
