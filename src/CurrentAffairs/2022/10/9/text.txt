










  ## ✅ हाल ही में स्वदेश में विकसित पहले हल्के लड़ाकू हेलीकॉप्टर(LCH) का नाम प्रचंड रखा गया है
![Prachand': IAF gets first made-in-India light combat helicopters today |  Mint](https://images.livemint.com/img/2022/10/03/1600x900/Prachand_1664781618006_1664781622991_1664781622991.PNG)

  ## ✅ हाल ही में ब्रिटेन देश ने अमीरों के लिए कर में कटौती के प्रस्ताव को रद्द कर दिया है
![UK Regions Map | United Kingdom Regions Map | Map of britain, Europe map, United  kingdom map](https://i.pinimg.com/originals/e5/36/76/e53676e71cf13beab6875da11952801d.gif)

  ## ✅ हाल ही में माधव हाड़ा को 'पंचम चोला पहर सखी री' पुस्तक के लिए 32वे बिहारी पुरस्कार से सम्मानित किया गया है
![Medieval Indian Literature | Madhav Hada](https://static.wixstatic.com/media/ceed95_d8bb5308b4024b9781aa508cc6825f82~mv2.jpg/v1/fill/w_560,h_746,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/IMG_8316_JPG.jpg)


  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/FwtYv8ZHJgvtR7yw7
