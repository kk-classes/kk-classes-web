















  ## ✅ हाल ही में मोहम्मद बीन सलमान सऊदी अरब के नए प्रधानमंत्री बने है
![Saudi Arabia's Crown Prince Mohammed bin Salman named prime minister |  World News | Sky News](https://e3.365dm.com/22/09/2048x1152/skynews-mohammed-bin-salman_5913157.jpg)

  ## ✅ हाल ही में ओडिशा राज्य 'जनजातीयों का विश्वकोश' जारी करने वाला पहला राज्य बना है
![Odisha District Map, Odisha Political Map](https://www.burningcompass.com/countries/india/maps/odisha-district-map.jpg)

  ## ✅ हाल ही में DRDO ने चाँदीपुर(ओडिशा) से कम दुरी पर मार करने वाली एयर डिफेंस सिस्टम मिसाइल 'VSHORADS' का सफलतापूर्वक परिक्षण किया है
![Successful Flight Tests of Very Short Range Air Defence System](https://blog.trishuldefenceacademy.com/wp-content/uploads/2022/10/Successful-Flight-Tests-of-Very-Short-Range-Air-Defence-System-VSHORADS-Missile-by-DRDO.jpg)

  ## ✅ हाल ही में चीन देश ने वैज्ञानिक प्रयोग करने और नई तकनीकों को सत्यापित करने के लिए 'शियान-14 और शियान-15' नाम के प्रायोगिक उपग्रहों को लांच किया है
![Political Map of China - Nations Online Project](https://www.nationsonline.org/maps/China-Political-Map.jpg)

  ## ✅ हाल ही में गिरिराज सिंह ने गांव में चयनित कुओं के जलस्तर का पता लगाने के लिए 'जलदूत' नामक ऐप को लांच किया है
![Giriraj Singh interview: 'Will work to awaken people towards nationalism' -  The Week](https://img.theweek.in/content/dam/week/news/india/images/2019/4/26/Giriraj-Singh-arvind-jain.jpg)

  ## ✅ हाल ही में प्रतिमा भौमिक ने भारतीय सांकेतिक भाषा के लिए 'साइन लर्न' नामक भाषा डिक्शनरी मोबाइल एप को लांच किया है
![Violence in Tripura pre-planned by Prashant, Abhishek: MoS Pratima Bhoumik](https://cdn.siasat.com/wp-content/uploads/2021/11/Pratima-Bhoumik-660x495.jpg)


  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/FwtYv8ZHJgvtR7yw7
