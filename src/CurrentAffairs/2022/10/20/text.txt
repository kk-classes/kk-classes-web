
  ## ✅ हाल ही में मध्य प्रदेश राज्य हिंदी में मेडिकल और इंजीनियरिंग की शिक्षा देने वाला भारत का पहला राज्य बन गया है
![MP District Map HD](https://www.burningcompass.com/countries/india/maps/madhya-pradesh-district-map-hd.jpg)

  ## ✅ हाल ही में जारी ग्लोबल हंगर इंडेक्स 2022 में भारत का स्थान 107वा है
![Global Hunger Index 2022: India slips six places, ranked 107 of 121  countries](https://affairscloud.com/assets/uploads/2022/10/Global-Hunger-Index-2022.jpg)

  ## ✅ हाल ही में NASA और ISRO दो एजेंसियो के बीच प्रोजेक्ट 'निसार' वर्ष 2023 में लांच होने वाला एक बड़ा प्रोजेक्ट है
![What is NISAR Mission? Is NASA ISRO Satellite "NISAR" Only About Natural  Geography ? - YouTube](https://i.ytimg.com/vi/h5OJf-8GbUU/maxresdefault.jpg)

  ## ✅ हाल ही में ISRO यूके के वैश्विक संचार नेटवर्क वनवेब के कुल 36 उपग्रहों को लांच करेगा
![ISRO Full Form, All You Need to Know About ISRO](https://st.adda247.com/https://s3-ap-south-1.amazonaws.com/adda247jobs-wp-assets-adda247/jobs/wp-content/uploads/sites/14/2021/10/27102924/ISRO-720x420-1.jpg)

  ## ✅ हाल ही में भारतीय सेना ने अग्निवीर सेल्लेरी पैकेज के लिए कुल 11 बैंको के साथ समझौता किया है
![Army signs MoUs with 11 banks for Agniveer salary Accounts: Major  developments under Agnipath Scheme |](https://newsonair.com/wp-content/uploads/2022/10/tulika-bishnoi-34.png)

  ## ✅ हाल ही में ओडिशा राज्य के मुख्यमंत्री ने 'फुटबॉल फॉर आल' कार्यक्रम शुरू किया है
![Odisha CM Launches 'Football For All' - Raman Academy](https://i0.wp.com/ramanacademy.in/wp-content/uploads/2022/10/1st-Indian-To-Win-Diamond-League-Trophy-Neeraj-Chopra-46.jpg)



  # **[प्रतिदिन प्रैक्टिस सेट]**
  👉 https://forms.gle/FwtYv8ZHJgvtR7yw7
