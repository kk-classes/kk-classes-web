import React, { useState, useCallback, useEffect } from 'react';
import { Redirect } from 'react-router';
import { useParams, useHistory } from 'react-router-dom';

import ImportantDates from 'Components/ImportantDates';
import importantDates from 'static/importantDates.json';

import moment from 'moment';

import {
  Container
} from 'react-bootstrap';

import MarkDownText from 'Components/MarkDownText';
import CurrentAffairsNavigator from 'Components/CurrentAffairsNavigator';

import './styles.scss';

function getURL(day, month, year) {
  return `/current-affairs/${parseInt(day)}/${parseInt(month)}/${parseInt(year)}/`
}

const CurrentAffairs = () => {
  const history = useHistory();
  let { date, month, year } = useParams();

  const gotoDate = useCallback(
    (day) => (
      moment(`${date}/${month}/${year}`, 'D/M/YYYY').add(day, "days").format('D/M/YYYY').split('/')
    ), [date, month, year]
  );

  const onChangeDate = useCallback(
    (changeDirection) => {
      const [day, month, year] = gotoDate(changeDirection);
      history.push(getURL(day, month, year));
    },
    [gotoDate, history]
  );

  if (!(date && month && year)) {
    // show the todays current affairs page
    const currentDate = moment();
    date = currentDate.date();
    month = currentDate.month() + 1;
    year = currentDate.year();

    return <Redirect to={getURL(date, month, year)} />
  }

  const onDateConfirm = (newDate) => {
    const [year, month, day] = newDate.split('-');
    history.push(getURL(day, month, year));
  }

  return (
    <>
      <div className="CANavigationWrapper">
        <CurrentAffairsNavigator
         date={date}
         month={month}
         year={year}
         onDateConfirm={onDateConfirm}
         onChangeDate={onChangeDate}
        />
      </div>
      <Container>
        <MarkDownText
          title="Daily Current Affairs"
          filePath={`CurrentAffairs/${year}/${month}/${date}/text.txt`}
        />
        <div className="importantDates">
          <ImportantDates
            title="महत्वपूर्ण दिवस"
            importantDateData={importantDates[month]}
          />
        </div>
      </Container>
    </>
  );
}

export default CurrentAffairs;
