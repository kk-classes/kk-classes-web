import React from 'react';

import {
  Container,
} from 'react-bootstrap';

import ImgHero1 from 'static/hero/hero-1.jpeg';
import ImgHero2 from 'static/hero/hero-2.jpeg';
import ImgHero3 from 'static/hero/hero-3.jpeg';

import HeroCarousel from 'Components/HeroCarousel';

import './styles.scss';

const Images = [
  {
    src: ImgHero1,
    title: "",
  },
  {
    src: ImgHero2,
    title: "",
  },
  {
    src: ImgHero3,
    title: "",
  },
];

const Home = () => {
  return (
    <div className="home-page">
      <HeroCarousel
        Images={Images}
      />
      <Container className="mt-5 quoteCard">
      <h2 className="text-center">OUR TEAM</h2>
      <ul>
        <li>
          KK Yadav (DIRECTOR) <small>(GK/GS Expert)</small> (दिल्ली)   
        </li>
        <li>              
          Shrikant Yadav (TECH HEAD) (दिल्ली)
        </li>
        <li>
          Shravan Yadav <small>(Management)</small>     
        </li>
        <li>
        Azad Khan <small>(GK/GS Expert)</small> (IAS Faculty)
        </li>
        <li>          
          Nazir Hussain <small>(Maths+Reasoning)</small> (पटना)       
        </li>
        <li>
          Devbrat Gupta <small>(Hindi)</small> (दिल्ली)          
        </li>
      </ul>
      </Container>
    </div>
  );
}

export default Home;
