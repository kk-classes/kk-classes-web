import {
  Container
} from 'react-bootstrap';

import MarkDownText from 'Components/MarkDownText';

import './styles.scss';

const CurrentAffairs = () => {
  return (
  <>
    <Container>
      <MarkDownText
        title=""
        filePath={`syllabus.txt`}
      />
    </Container>
  </>
  );
}

export default CurrentAffairs;
